# Superchain

Superchain is a Blockchain platform that aims to fuse Blockchain with Cloud Computing. With Superchain, you get a interface similar to AWS or Google Cloud: a cli, web console, services like identity, storage, functions, pubsub, etc ... Along with that you also get the immutability, censor resistance, distributed nature of Blockchain. We are building Superchain with ease of use, security and scalability in mind so that you can build apps like twitter, facebook and snapchat with ease.


## Setup
* install virtual env using: sudo pip install virtualenv
* mkdir ~/.envs; cd ~/.envs
* create a virtual env: virtualenv -p python3 env3
* activate a virtual env: source ~/.envs/env3/bin/activate
* go to superchain folder: cd ~/code/superchain/superchainpy
* install requirements: pip install -r requirements.txt
* install superchain in development mode: python setup.py develop
* start server: superchaind start
* invoke cli: cloud


## Roadmap
* December 2018: Implement identity, wallets, storage and pubsub
* June 2019: Create Python, Dart & JavaScript SDKs for Superchain
* December 2019: Mobile and Web Apps based on Superchain
