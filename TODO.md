# TODO

## Rest API
* Implement all cli commands using REST API
* Create a shared Postman collection for API (backups) or swagger.

## Consensus
* Merkletree with Proof-of-Stake

## Config
* Make backend configurable: url
* local vs cloud vs distributed

## gRPC
* Define Protocol Buffers
* Implement gRPC interfaces

## Cloud Backend
* implement with Google Cloud

## Distributed Implementations
* Research on Distributed Systems

## Docs
* md format
* readthedocs integration
* migrate to GitHub pages for documentation

## Website
* Simple one page theme

## PubSub
* Implement events standard: https://medium.com/@austencollins/introducing-cloudevents-a758c62c76bf

## Community
* Demo to Dheeraj and Dan Trevino
* Get feedback and polish API structure and responses
* Build node.js library
* Template for an electron app / webapp
* Let Dheeraj build 100 apps
