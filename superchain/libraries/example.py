from pubsub import PersistentQueue

q1 = PersistentQueue("/tmp/queue_storage_dir")
q1.put("1")
q1.put("2")
q1.put("3")
q1.close()

q2 = PersistentQueue("/tmp/queue_storage_dir")
while not q2.empty():
    print repr(q2.get())
