import json
import click
import requests

url = 'http://localhost:5000/{0}'


def is_cs_path(path):
    if path and len(path) > 5 and path[:5] == 'cs://':
        return True
    return False

def is_local_path(path):
    return not is_cs_path(path)

def to_local_path(path):
    from os.path import expanduser
    if not path:
        return expanduser('~/.superchain/storage/')
    # strip protocol prefix, if it exists
    if len(path) >= 5 and path[:5] == 'cs://':
        path = path[5:]
    path = expanduser('~/.superchain/storage/{0}'.format(path))
    return path


@click.group()
@click.option('-f', '--format', default=None, help='Output format')
def storage(format): pass

@storage.command('create')
@click.argument('path', required=True)
@click.option('-f', '--format', default=None, help='Output format')
def storage_create(path, format):
    data = {
        'OPERATION': 'CREATE',
        'path': path
    }
    response = requests.post(url.format('storage/'), data=json.dumps(data)).json()
    if response['status'] == 'error':
        click.echo('Error creating {0}: {1}'.format(path, response['message']))
    elif response['status'] == 'success':
        click.echo('Created {0}'.format(response['message']))
    else:
        click.echo('Unknown response: {0}'.format(response))


@storage.command('ls')
@click.argument('path', required=False)
@click.option('-f', '--format', default=None, help='Output format')
def storage_list(path, format):
    if not path: path = 'cs://'
    params = {'path': path}
    response = requests.get(url.format('storage/'), params=params).json()
    if response['status'] == 'error':
        click.echo('Error listing {0}: {1}'.format(path, response['message']))
    elif response['status'] == 'success':
        for object in response['directory']:
            click.echo('{0}'.format(object['path']))
    else:
        click.echo('Unknown response: {0}'.format(response))


@storage.command('cp')
@click.argument('source', required=True)
@click.argument('destination', required=True)
@click.option('-f', '--format', default=None, help='Output format')
def storage_cp(source, destination, format):
    if is_local_path(source) and is_cs_path(destination):
        # local file to remote file
        content = open(source, 'r').read()
        data = {
            'OPERATION': 'LOCAL_TO_REMOTE',
            'source': source,
            'destination': destination,
            'file': content
        }
        response = requests.post(url.format('storage/'), data=json.dumps(data)).json()
        if response['status'] == 'error':
            click.echo('Error: {0}.'.format(response['message']))
        elif response['status'] == 'success':
            click.echo('Copied {0} to {1}'.format(source, destination))
        else:
            click.echo('Unknown response: {0}'.format(response))
    elif is_cs_path(source) and is_local_path(destination):
        # copy from remote file to local file
        data = {
            'OPERATION': 'REMOTE_TO_LOCAL',
            'source': source,
            'destination': destination,
        }
        response = requests.post(url.format('storage/'), data=json.dumps(data)).json()
        if response['status'] == 'error':
            click.echo('Error: {0}.'.format(response['message']))
        elif response['status'] == 'success':
            content = response['file']
            with open(destination, 'w') as f:
                f.write(content)
            click.echo('Copied {0} to {1}'.format(source, destination))
        else:
            click.echo('Unknown response: {0}'.format(response))
    elif is_cs_path(source) and is_cs_path(destination):
        # copy from remote file to remote file
        data = {
            'OPERATION': 'REMOTE_TO_REMOTE',
            'source': source,
            'destination': destination
        }
        response = requests.post(url.format('storage/'), data=json.dumps(data)).json()
        if response['status'] == 'error':
            click.echo('Error: {0}.'.format(response['message']))
        elif response['status'] == 'success':
            click.echo('Copied {0} to {1}'.format(source, destination))
        else:
            click.echo('Unknown response: {0}'.format(response))
    elif is_local_path(source) and is_local_path(destination):
        # copy local file to remote file
        from shutil import copyfile
        copyfile(source, destination)
    else:
        click.echo('Error copying {0} to {1}'.format(source, destination))


@storage.command('rm')
@click.argument('path', required=True)
@click.option('-f', '--format', default=None, help='Output format')
def storage_remove(path, format):
    data = {'path': path}
    response = requests.delete(url.format('storage/'), data=json.dumps(data)).json()
    if response['status'] == 'error':
        click.echo('Error removing the path {0}: {1}'.format(path, response['message']))
    elif response['status'] == 'success':
        click.echo('Removed path {0}'.format(path))
    else:
        click.echo('Unknown response: {0}'.format(response))
