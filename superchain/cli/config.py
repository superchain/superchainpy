import json
import click
import requests

url = 'http://localhost:5000/{0}'


@click.group()
@click.option('-f', '--format', default=None, help='Output format')
def config(format): pass

@config.command('ls')
@click.option('-f', '--format', default=None, help='Output format')
def config_list(format):
    config = requests.get(url.format('config')).json()
    print('config:')
    for key, value in config.items():
        print('\t{0}\t{1}'.format(key, value))

@config.command('set')
@click.argument('key', required=True)
@click.argument('value', required=True)
@click.option('-f', '--format', default=None, help='Output format')
def config_set(key, value, format):
    response = requests.post(url.format('config'), data=json.dumps({'key':key, 'value':value})).json()
    print(response)

@config.command('get')
@click.argument('key', required=True)
@click.option('-f', '--format', default=None, help='Output format')
def config_get(key, format):
    config = requests.get(url.format('config')).json()
    sybsystem, key = key.split(':')
    print(config[subsystem][key])
