import json
import click
import requests

url = 'http://localhost:5000/{0}'


@click.group()
@click.option('-f', '--format', default=None, help='Output format')
def init(format):
    # if any identities exist, try to login as one of the existing identities
    identities = requests.get(url.format('identity')).json()
    if identities:
        print('Found Identities:')
        for identity in identities:
            print('{0}'.format(identity))
        identity = input('Login as Identity: ')
    # if no identities exist, create a new identity
    else:
        identity = input('Enter the name for new identity: ')

    response = requests.post(url.format('config'), data=json.dumps({'key':'identity', 'value':identity})).json()
    if response['status'] == 'error':
        click.echo('Error: {0}.'.format(response['message']))
    elif response['status'] == 'success':
        click.echo('Logged in as identity: {0}'.format(identity))
    else:
        click.echo('Unknown response: {0}'.format(response))
