import json
import click
import requests

url = 'http://localhost:5000/{0}'


@click.group()
@click.option('-f', '--format', default=None, help='Output format')
def ls(format): pass


@ls.command('orgs')
@click.option('-f', '--format', default=None, help='Output format')
def list_organizations(format):
    response = requests.get(url.format('iam/organization/'))

    if response.status_code == requests.codes.ok:
        if format and format == 'json':
            click.echo(response.json())
        else:
            for org in response.json():
                click.echo(org)
    elif response.status_code == requests.codes.bad:
        click.echo('Error: {0}.'.format(response.content))
    else:
        click.echo('Unknown response: {0}'.format(response))


@ls.command('folders')
@click.option('-f', '--format', default=None, help='Output format')
def list_folders(format):
    response = requests.get(url.format('iam/folder/'))

    if response.status_code == requests.codes.ok:
        if format and format == 'json':
            click.echo(response.json())
        else:
            for folder in response.json():
                click.echo(folder)
    elif response.status_code == requests.codes.bad:
        click.echo('Error: {0}.'.format(response.content))
    else:
        click.echo('Unknown response: {0}'.format(response))


@ls.command('projects')
@click.option('-f', '--format', default=None, help='Output format')
def list_projects(format):
    response = requests.get(url.format('iam/project/'))

    if response.status_code == requests.codes.ok:
        if format and format == 'json':
            click.echo(response.json())
        else:
            for project in response.json():
                click.echo(project)
    elif response.status_code == requests.codes.bad:
        click.echo('Error: {0}.'.format(response.content))
    else:
        click.echo('Unknown response: {0}'.format(response))


@ls.command('identities')
@click.option('-f', '--format', default=None, help='Output format')
def list_identities(format):
    response = requests.get(url.format('iam/identity/'))

    if response.status_code == requests.codes.ok:
        if format and format == 'json':
            click.echo(response.json())
        else:
            for identity in response.json():
                click.echo(identity)
    elif response.status_code == requests.codes.bad:
        click.echo('Error: {0}.'.format(response.content))
    else:
        click.echo('Unknown response: {0}'.format(response))


@ls.command('roles')
@click.option('-f', '--format', default=None, help='Output format')
def list_roles(format):
    response = requests.get(url.format('iam/role/'))

    if response.status_code == requests.codes.ok:
        if format and format == 'json':
            click.echo(response.json())
        else:
            for role in response.json():
                click.echo(role)
    elif response.status_code == requests.codes.bad:
        click.echo('Error: {0}.'.format(response.content))
    else:
        click.echo('Unknown response: {0}'.format(response))
