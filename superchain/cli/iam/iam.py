# -*- coding: utf-8 -*-

"""Identity and Access Management."""
import click

from superchain.cli.iam.assign import assign
from superchain.cli.iam.create import create
from superchain.cli.iam.edit import edit
from superchain.cli.iam.ls import ls
from superchain.cli.iam.rm import rm


@click.group()
@click.option('-f', '--format', default=None, help='Output format')
def iam(format): pass

iam.add_command(assign)
iam.add_command(create)
iam.add_command(edit)
iam.add_command(ls)
iam.add_command(rm)
