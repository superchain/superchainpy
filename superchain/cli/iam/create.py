import os
import json
import click
import requests

url = 'http://localhost:5000/{0}'
identity_config_file = os.path.expanduser("~/.superchain/identity/config.yaml")

@click.group()
@click.option('-f', '--format', default=None, help='Output format')
def create(format): pass


@create.command('folder')
@click.argument('folder', required=True)
@click.option('-f', '--format', default=None, help='Output format')
def create_folder(folder, format):
    response = requests.post(url.format('iam/folder/{0}'.format(folder)))
    if response.status_code == requests.codes.created:
        click.echo('{0}'.format(response.content))
    elif response.status_code == requests.codes.bad:
        click.echo('Error: {0}.'.format(response.content))
    else:
        click.echo('Unknown response: {0}'.format(response))


@create.command('identity')
@click.argument('identity', required=True)
@click.option('-w', '--wallet', default=None, help='Import Wallet')
@click.option('-f', '--format', default=None, help='Output format')
def create_identity(identity, wallet, format):
    if wallet:
        # import wallet if specified
        with open(wallet) as w:
            data = json.load(w)
            address = data['address']
    else:
        # create new address
        address = 'A1DKSDHGSHJSGDKJSKJDKJ'

    data = {
        'address': address
    }

    response = requests.post(url.format('iam/identity/{0}'.format(identity)), data=json.dumps(data))
    if response.status_code == requests.codes.created:
        click.echo('{0}'.format(response.content))
    elif response.status_code == requests.codes.bad:
        click.echo('Error: {0}.'.format(response.content))
    else:
        click.echo('Unknown response: {0}'.format(response))


@create.command('org')
@click.argument('org', required=True)
@click.option('-c', '--config', default=None, help='Config file for Organization')
@click.option('-f', '--format', default=None, help='Output format')
def create_organization(org, format):
    stream = open(identity_config_file)
    config = yaml.load(stream)
    stream.close()

    data = {
        'name': 'Organization',
        'owners': [config['identity']],
        'shares': {}
    }

    response = requests.post(url.format('iam/organization/{0}'.format(org)), data=json.dumps(data))
    if response.status_code == requests.codes.created:
        click.echo('{0}'.format(response.content))
    elif response.status_code == requests.codes.bad:
        click.echo('Error: {0}.'.format(response.content))
    else:
        click.echo('Unknown response: {0}'.format(response))


@create.command('project')
@click.argument('project', required=True)
@click.option('-f', '--format', default=None, help='Output format')
def create_project(project, format):
    data = {
        'org': 'Organization',
        'name': 'Project',
        'iam': {}
    }

    response = requests.post(url.format('iam/project/{0}'.format(project)), data=json.dumps(data))
    if response.status_code == requests.codes.created:
        click.echo('{0}'.format(response.content))
    elif response.status_code == requests.codes.bad:
        click.echo('Error: {0}.'.format(response.content))
    else:
        click.echo('Unknown response: {0}'.format(response))


@create.command('role')
@click.argument('role', required=True)
@click.option('-f', '--format', default=None, help='Output format')
def create_role(role, format):
    response = requests.post(url.format('iam/role/{0}'.format(role)))
    if response.status_code == requests.codes.created:
        click.echo('{0}'.format(response.content))
    elif response.status_code == requests.codes.bad:
        click.echo('Error: {0}.'.format(response.content))
    else:
        click.echo('Unknown response: {0}'.format(response))
