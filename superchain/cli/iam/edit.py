import json
import click
import requests

url = 'http://localhost:5000/{0}'


@click.group()
@click.option('-f', '--format', default=None, help='Output format')
def edit(format): pass


@edit.command('folder')
@click.option('-f', '--format', default=None, help='Output format')
def create_folder(folder, format): pass


@edit.command('identity')
@click.option('-f', '--format', default=None, help='Output format')
def create_folder(identity, format): pass


@edit.command('organization')
@click.option('-f', '--format', default=None, help='Output format')
def create_folder(org, format): pass


@edit.command('project')
@click.option('-f', '--format', default=None, help='Output format')
def create_project(project, format): pass


@edit.command('role')
@click.option('-f', '--format', default=None, help='Output format')
def create_folder(role, format): pass
