import json
import click
import requests

url = 'http://localhost:5000/{0}'


@click.group()
@click.option('-f', '--format', default=None, help='Output format')
def rm(format): pass


@rm.command('orgs')
@click.argument('org', required=True)
@click.option('-f', '--format', default=None, help='Output format')
def rm_organization(org, format):
    response = requests.delete(url.format('iam/organization/{0}'.format(org)))

    if response.status_code == requests.codes.ok:
        click.echo(response.content)
    elif response.status_code == requests.codes.bad:
        click.echo('Error: {0}.'.format(response.content))
    else:
        click.echo('Unknown response: {0}'.format(response))


@rm.command('folders')
@click.argument('folder', required=True)
@click.option('-f', '--format', default=None, help='Output format')
def rm_folder(folder, format):
    response = requests.delete(url.format('iam/folder/{0}'.format(folder)))

    if response.status_code == requests.codes.ok:
        click.echo(response.content)
    elif response.status_code == requests.codes.bad:
        click.echo('Error: {0}.'.format(response.content))
    else:
        click.echo('Unknown response: {0}'.format(response))


@rm.command('projects')
@click.argument('project', required=True)
@click.option('-f', '--format', default=None, help='Output format')
def rm_project(project, format):
    response = requests.delete(url.format('iam/project/{0}'.format(project)))

    if response.status_code == requests.codes.ok:
        click.echo(response.content)
    elif response.status_code == requests.codes.bad:
        click.echo('Error: {0}.'.format(response.content))
    else:
        click.echo('Unknown response: {0}'.format(response))


@rm.command('identity')
@click.argument('identity', required=True)
@click.option('-f', '--format', default=None, help='Output format')
def remove_identity(identity, format):
    response = requests.delete(url.format('iam/identity/{0}'.format(identity)))

    if response.status_code == requests.codes.ok:
        click.echo(response.content)
    elif response.status_code == requests.codes.bad:
        click.echo('Error: {0}.'.format(response.content))
    else:
        click.echo('Unknown response: {0}'.format(response))


@rm.command('roles')
@click.argument('identity', required=True)
@click.option('-f', '--format', default=None, help='Output format')
def rm_role(role, format):
    response = requests.delete(url.format('iam/role/{0}'.format(role)))

    if response.status_code == requests.codes.ok:
        click.echo(response.content)
    elif response.status_code == requests.codes.bad:
        click.echo('Error: {0}.'.format(response.content))
    else:
        click.echo('Unknown response: {0}'.format(response))
