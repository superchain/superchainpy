import json
import click
import requests

url = 'http://localhost:5000/{0}'


@click.group()
@click.option('-f', '--format', default=None, help='Output format')
def functions(format): pass


@functions.command('create')
@click.argument('function', required=True)
@click.argument('path', required=True)
@click.option('-f', '--format', default=None, help='Output format')
def function_create(function, path, format):
    import dill
    import function as f
    code = dill.dumps(f)
    data = {
        'OPERATION': 'CREATE_FUNCTION',
        'code': code
    }
    response = requests.post(url.format('functions/{0}'.format(function)), data=code).json()
    if response['status'] == 'error':
        click.echo('Error creating function {0}: {1}'.format(function, response['message']))
    elif response['status'] == 'success':
        click.echo('{0}.'.format(response['message']))
    else:
        click.echo('Unknown response: {0}'.format(response))


@functions.command('ls')
@click.option('-f', '--format', default=None, help='Output format')
def functions_list(format):
    response = requests.get(url.format('functions/')).json()
    if response['status'] == 'error':
        click.echo('Error listing functions: {0}.'.format(response['message']))
    elif response['status'] == 'success':
        for function in response['functions']:
            click.echo('{0}'.format(function))
    else:
        click.echo('Unknown response: {0}'.format(response))


# migrate functions interface to confirm to events interface
@functions.command('invoke')
@click.argument('function', required=True)
@click.argument('x', required=True)
@click.argument('y', required=True)
@click.option('-f', '--format', default=None, help='Output format')
def functions_invoke(function, x, y, format):
    data = {
        'OPERATION': 'INVOKE_FUNCTION',
        'function': function,
        'x': x,
        'y': y
    }
    response = requests.post(url.format('functions/{0}'.format(function)), data=json.dumps(data)).json()
    if response['status'] == 'error':
        click.echo('Error invoking function {0}: {1}'.format(function, response['message']))
    elif response['status'] == 'success':
        click.echo('{0}'.format(response['result']))
    else:
        click.echo('Unknown response: {0}'.format(response))


@functions.command('rm')
@click.argument('function', required=True)
@click.option('-f', '--format', default=None, help='Output format')
def functions_remove(function, format):
    response = requests.delete(url.format('functions/{0}'.format(function))).json()
    if response['status'] == 'success':
        click.echo('Removed function:{0}'.format(function))
    else:
        print(response['message'])
