import json
import click
import requests

url = 'http://localhost:5000/{0}'


@click.group()
@click.option('-f', '--format', default=None, help='Output format')
def wallet(format): pass


@wallet.command('create')
@click.argument('wallet', required=True)
@click.option('-f', '--format', default=None, help='Output format')
def wallet_create(wallet, format):
    click.echo('chain wallet create {0}'.format(wallet))

@wallet.command('ls')
@click.option('-f', '--format', default=None, help='Output format')
def wallet_list(format):
    for identity in db['identities']:
        print('{0}: '.format(identity['user']))
        for account in db['accounts']:
            if identity['user'] == account['user']:
                print('\tAddress: {0}\tBalance: {1}'.format(account['address'], account['balance']))


@wallet.command('send')
@click.argument('amount', required=True)
@click.argument('address', required=True)
@click.option('-f', '--format', default=None, help='Output format')
def wallet_send(amount, address, format):
    amount = float(amount)
    # get active identity
    config = db['config'].all()
    for cfg in config:
        if cfg['key'] == 'user':
            user = cfg['value']
            break

    # check the balance
    balance = 0
    for account in db['accounts']:
        if user == account['user'] and balance < amount + fees:
            balance = balance + account['balance']
    if balance < amount + fees:
        click.echo('Insufficient Balance!')
        return 1

    # deduct the balance
    balance = amount + fees
    for account in db['accounts']:
        if user == account['user'] and account['balance'] < balance:
            balance = balance - account['balance']
            db['accounts'].update(dict(address=account['address'], balance=0.0), ['address'])
        elif user == account['user'] and account['balance'] >= balance:
            db['accounts'].update(dict(
                address=account['address'],
                balance=account['balance'] - balance
            ), ['address'])
            break

    # credit the balance
    for account in db['accounts']:
        if address == account['address']:
            db['accounts'].update(dict(address=address, balance=account['balance']+amount), ['address'])

    return 0

@wallet.command('rm')
@click.argument('wallet', required=True)
@click.option('-f', '--format', default=None, help='Output format')
def wallet_remove(wallet, format):
    click.echo('chain wallet remove {0}'.format(wallet))
