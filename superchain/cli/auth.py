import json
import yaml
import click
import requests
from os.path import expanduser

config_file = expanduser("~/.superchain/identity/config.yaml")

@click.group()
@click.option('-f', '--format', default=None, help='Output format')
def auth(format): pass

@auth.command('login')
@click.argument('identity', required=True)
@click.option('-f', '--format', default=None, help='Output format')
def auth_login(identity, format):
    stream = open(config_file)
    config = yaml.load(stream)
    stream.close()

    stream = open(config_file, 'w')
    config['identity'] = identity
    yaml.dump(config, stream)
    stream.close()

    click.echo('Logged in to account: {0}'.format(identity))

@auth.command('logout')
@click.option('-f', '--format', default=None, help='Output format')
def auth_logout(format):
    stream = open(config_file)
    config = yaml.load(stream)
    identity = config['identity']
    stream.close()

    stream = open(config_file, 'w')
    config['identity'] = None
    yaml.dump(config, stream)
    stream.close()

    click.echo('Logged out of account: {0}'.format(identity))
