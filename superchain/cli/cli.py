# -*- coding: utf-8 -*-

"""Console script for superchain."""
import sys
import click

from superchain.cli.auth import auth
from superchain.cli.config import config
from superchain.cli.contracts import contracts
from superchain.cli.functions import functions
from superchain.cli.iam.iam import iam
from superchain.cli.init import init
from superchain.cli.pubsub import pubsub
from superchain.cli.storage import storage
from superchain.cli.wallet import wallet

@click.group()
@click.option('-f', '--format', default=None, help='Output format')
def main(format): pass

main.add_command(auth)
main.add_command(config)
main.add_command(contracts)
main.add_command(functions)
main.add_command(iam)
main.add_command(init)
main.add_command(pubsub)
main.add_command(storage)
main.add_command(wallet)


if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
