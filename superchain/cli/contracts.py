import json
import click
import requests

url = 'http://localhost:5000/{0}'


@click.group()
@click.option('-f', '--format', default=None, help='Output format')
def contracts(format): pass

@contracts.command('create')
@click.argument('contract', required=True)
@click.argument('path', required=True)
@click.option('-f', '--format', default=None, help='Output format')
def contracts_create(contract, path, format):
    response = requests.post(url.format('contracts/{0}'.format(contract))).json()
    click.echo('chain contracts create contract:{0}'.format(contract))

@contracts.command('ls')
@click.option('-f', '--format', default=None, help='Output format')
def contracts_list(format):
    response = requests.get(url.format('contracts/'))
    click.echo(response)

@contracts.command('invoke')
@click.argument('contract', required=True)
@click.argument('parameters', required=True)
@click.option('-f', '--format', default=None, help='Output format')
def contracts_publish(contract, parameters, format):
    click.echo('chain contracts invoke contract:{0} with parameters:{1}'.format(contract, parameters))

@contracts.command('rm')
@click.argument('contract', required=True)
@click.option('-f', '--format', default=None, help='Output format')
def contracts_remove(contract, format):
    response = requests.delete(url.format('contracts/{0}'.format(contract))).json()
    click.echo('chain contracts remove contract:{0}'.format(contract))
