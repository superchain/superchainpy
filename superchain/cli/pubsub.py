import json
import click
import requests

url = 'http://localhost:5000/{0}'


@click.group()
@click.option('-f', '--format', default=None, help='Output format')
def pubsub(format): pass


def to_pubsub_path(topic):
    from os.path import expanduser
    return expanduser('~/.superchain/pubsub/{0}'.format(topic))


@pubsub.command('create')
@click.argument('topic_name', required=True)
@click.argument('subscription', required=False)
@click.option('-f', '--format', default=None, help='Output format')
def pubsub_create(topic_name, subscription, format):
    if topic_name and not subscription:
        data = {
            'OPERATION': 'CREATE_TOPIC',
            'topic_name': topic_name,
            'subscription': subscription
        }
        response = requests.post(url.format('pubsub/{0}'.format(topic_name)), data=json.dumps(data)).json()
        if response['status'] == 'error':
            click.echo('Error: {0}.'.format(response['message']))
        elif response['status'] == 'success':
            click.echo('Created topic {0}'.format(topic_name))
        else:
            click.echo('Unknown response: {0}'.format(response))
    else:
        click.echo('creating subscriptions is not allowed yet!')


@pubsub.command('ls')
@click.option('-f', '--format', default=None, help='Output format')
def pubsub_list(format):
    response = requests.get(url.format('pubsub/')).json()
    if response['status'] == 'error':
        click.echo('Error listing topics: {0}.'.format(response['message']))
    elif response['status'] == 'success':
        for topic in response['topics']:
            click.echo('{0}'.format(topic))
    else:
        click.echo('Unknown response: {0}'.format(response))


@pubsub.command('write')
@click.argument('topic_name', required=True)
@click.argument('message', required=True)
@click.option('-f', '--format', default=None, help='Output format')
def pubsub_write(topic_name, message, format):
    data = {
        'OPERATION': 'WRITE_MESSAGE',
        'topic_name': topic_name,
        'message': message
    }
    response = requests.post(url.format('pubsub/{0}'.format(topic_name)), data=json.dumps(data)).json()
    click.echo('Published message {0} to topic {1}'.format(message, topic_name))


@pubsub.command('read')
@click.argument('topic_name', required=True)
@click.option('-f', '--format', default=None, help='Output format')
def pubsub_read(topic_name, format):
    response = requests.get(url.format('pubsub/{0}'.format(topic_name))).json()
    for message in response['messages']:
        click.echo(message)


@pubsub.command('rm')
@click.argument('topic_name', required=True)
@click.option('-f', '--format', default=None, help='Output format')
def pubsub_remove(topic_name, format):
    response = requests.delete(url.format('pubsub/{0}'.format(topic_name))).json()
    if response['status'] == 'success':
        click.echo('Removed topic:{0}'.format(topic_name))
    else:
        print(response['message'])
