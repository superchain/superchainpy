import os
import yaml
import json
import shutil

from flask import jsonify
from flask import request
from flask import Blueprint

from os.path import expanduser

functions_api = Blueprint('functions_api', __name__)


"""Superchaind API: Functions."""
functions = {}
@functions_api.route('/functions/', methods=['GET'])
def functions_get():
    data = {
        'status': 'success',
        'functions': []
    }
    for function in functions:
        data['functions'].append(function)
    return jsonify(data)


@functions_api.route('/functions/<function>', methods=['POST'])
def functions_post(function):
    action = 'CREATE_FUNCTION'
    try:
        data = json.loads(request.data)
        action = data['OPERATION']
    except:
        pass

    if action == 'CREATE_FUNCTION':
        if function in functions:
            data = {'status': 'error', 'message': 'Function already exists.'}
            return jsonify(data)

        functions[function] = request.data
        return jsonify({'status': 'success', 'message': 'Created function {0}'.format(function)})
    elif action == 'INVOKE_FUNCTION':
        if function not in functions:
            data = {'status': 'error', 'message': 'Function does not exist.'}
            return jsonify(data)

        import dill
        f = dill.loads(functions[function])
        x, y = int(data['x']), int(data['y'])
        data = {'status': 'success', 'result': f.function(x, y)}
        return jsonify(data)
    else:
        pass


@functions_api.route('/functions/<function>', methods=['DELETE'])
def functions_remove(function):
    if not function.isalnum():
        return jsonify({'status': 'error', 'message': 'Invalid function name: {0}'.format(function)})

    if function not in functions:
        return jsonify({'status': 'error', 'message': 'Function : {0} does not exist'.format(function)})
    del functions[function]
    return jsonify({'status': 'success', 'message': 'Deleted function: {0}'.format(function)})
