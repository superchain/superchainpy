import os
import sys
import time
import copy
import json
import yaml
import queue
import shutil
import hashlib
import requests
import threading
import datetime

from flask import Flask
from flask import request
from flask import jsonify

from os.path import expanduser

from superchain.server.iam import iam_api
from superchain.server.config import config_api
from superchain.server.storage import storage_api
from superchain.server.pubsub import pubsub_api
from superchain.server.wallet import wallet_api
from superchain.server.contracts import contracts_api
from superchain.server.functions import functions_api

app = Flask(__name__)
app.register_blueprint(iam_api)
app.register_blueprint(config_api)
app.register_blueprint(storage_api)
app.register_blueprint(pubsub_api)
app.register_blueprint(wallet_api)
app.register_blueprint(contracts_api)
app.register_blueprint(functions_api)

"""Superchaind Miner."""
fees = 1.0

memory = {
    'config': {},
    'identity': {},
    'wallets': {}
}

state = {
    'config': {},
    'identity': {},
    'wallets': {}
}
blockchain = []

def initialize():
    # create base directory
    from os.path import expanduser
    home = expanduser("~/.superchain/")
    if os.path.exists(home):
        return
    os.makedirs(home)

    # create config file and write default config
    config = {
        'chain': {
            'name': 'superchain',
            'host': 'htpp://localhost',
            'port': 5000
        },
        'identity': {}
    }

    stream = open(home + 'config.yaml', 'w')
    yaml.dump(config, stream)

    # create module directories
    os.makedirs(home + 'identity/')
    os.makedirs(home + 'wallet/')
    os.makedirs(home + 'storage/')
    os.makedirs(home + 'pubsub/')
    os.makedirs(home + 'contracts/')
    os.makedirs(home + 'functions/')

def hash(data):
    sha = hashlib.sha256()
    sha.update(str(data).encode('utf-8'))
    return sha.hexdigest()

def create_genesis_block():
    block = {
        'index': 0,
        'timestamp': datetime.datetime.now(),
        'data': memory,
        'previous': ''
    }
    block['hash'] = hash(block)
    return block

def Block(previous):
    global memory
    block = {
        'index': previous['index'] + 1,
        'timestamp': datetime.datetime.now(),
        'data': copy.copy(memory),
        'previous': previous['hash']
    }

    # update state
    for subsystem in memory:
        for key, value in memory[subsystem].items():
            if value['OPCODE'] == 'CREATE':
                state[subsystem][key] = value
            elif value['OPCODE'] == 'DELETE':
                del state[subsystem][key]
            else:
                'Unknown OPCODE'

    # update hash
    block['hash'] = hash(block)
    # purge memory
    for key in memory:
        memory[key] = {}

    return block

class Miner(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def stop(self):
        self._stop_event.set()

    def run(self):
        block = create_genesis_block()
        print(block)
        blockchain.append(block)
        while True:
            time.sleep(1)
            flag = False
            for subsystem in memory:
                if memory[subsystem]:
                    flag = True
                    break
            if flag:
                block = Block(blockchain[-1])
                print(block)
                blockchain.append(block)


"""Superchaind API."""

"""Superchaind API: Hello, World!."""
@app.route("/")
def hello():
    data = {'status': 'success', 'message': 'Hello, World!'}
    return jsonify(data)


"""Superchaind API: Server."""
class Server(threading.Thread):
    def stop(self):
        self._stop_event.set()

    def run(self):
        app.run()


if __name__ == '__main__':
    initialize()
    server = Server()
    miner = Miner()
    server.start()
    miner.start()
