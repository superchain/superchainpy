import os
import yaml
import json
import shutil

from flask import jsonify
from flask import request
from flask import Blueprint

from os.path import expanduser

storage_api = Blueprint('storage_api', __name__)


"""Superchaind API: Storage."""
def is_cs_path(path):
    if path and len(path) > 5 and path[:5] == 'cs://':
        return True
    return False

def to_local_path(path):
    if not path:
        return expanduser('~/.superchain/storage/')
    # strip protocol prefix, if it exists
    if len(path) >= 5 and path[:5] == 'cs://':
        path = path[5:]
    path = expanduser('~/.superchain/storage/{0}'.format(path))
    return path

def to_cs_path(path):
    return 'cs://' + path


@storage_api.route('/storage/', methods=['GET'])
def storage_get():
    path = request.args.get('path', 'cs://')
    data = {}
    local_path = to_local_path(path)
    if not os.path.exists(local_path):
        data = {'status': 'error', 'message': 'Requested path does not exist.'}
    elif os.path.isdir(local_path):
        if path[-1] != '/': path = path + '/'
        data['status'] = 'success'
        data['directory'] = []
        contents = os.listdir(local_path)
        for content in contents:
            data['directory'].append({'path':path + content})
    elif os.path.isfile(local_path):
        data['status'] = 'success'
        data['file'] = open(local_path, 'r').read()
    else:
        data['status'] = 'error'
        data['message'] = 'Unknown object type'

    return jsonify(data)


@storage_api.route('/storage/', methods=['POST'])
def storage_post():
    data = json.loads(request.data)
    if data['OPERATION'] == 'CREATE':
        local_path = to_local_path(data['path'])
        if os.path.exists(local_path):
            response = {'status': 'error', 'message': 'Object: {0} already exists.'.format(data['path'])}
            return jsonify(response)
        os.makedirs(local_path)
        response = {'status': 'success', 'message': 'Created {0}'.format(data['path'])}
        return jsonify(response)
    elif data['OPERATION'] == 'LOCAL_TO_REMOTE':
        content = data['file']
        local_path = to_local_path(data['destination'])
        with open(local_path, 'w') as f:
            f.write(content)
        return jsonify({'status': 'success', 'message': 'Copied the file {0} to {1}'.format(data['source'], data['destination'])})
    elif data['OPERATION'] == 'REMOTE_TO_LOCAL':
        local_path = to_local_path(data['source'])
        if not os.path.exists(local_path):
            response = {'status': 'error', 'message': 'Object: {0} does not exist.'.format(data['source'])}
            return jsonify(response)
        content = open(local_path, 'r').read()
        data = {'status': 'success', 'file': content}
        return jsonify(data)
    elif data['OPERATION'] == 'REMOTE_TO_REMOTE':
        source = to_local_path(data['source'])
        destination = to_local_path(data['destination'])
        if not os.path.exists(source):
            response = {'status': 'error', 'message': 'Object: {0} does not exist.'.format(data['source'])}
            return jsonify(response)
        from shutil import copyfile
        copyfile(source, destination)
        data = {'status': 'success', 'message': 'Copied {0} to {1}'.format(data['source'], data['destination'])}
        return jsonify(data)
    else:
        pass # error


@storage_api.route('/storage/', methods=['DELETE'])
def storage_remove():
    data = json.loads(request.data)
    path = to_local_path(data['path'])
    if not os.path.exists(path):
        return jsonify({'status': 'error', 'message': '{0} does not exists'.format(data['path'])})
    shutil.rmtree(path)
    data = {
        'status': 'success', 'message' : 'Deleted {0}'.format(data['path'])
    }
    return jsonify(data)
