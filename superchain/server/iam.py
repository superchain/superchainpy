import os
import yaml
import json
import shutil
from os.path import expanduser

from flask import jsonify
from flask import request
from flask import Blueprint
from flask_api import status

from superchain.server.state import state

iam_api = Blueprint('iam_api', __name__)


"""Superchaind API: IAM."""

@iam_api.route('/iam/organization/', methods=['GET'])
def list_organizations():
    content = state['iam']['organizations']
    return jsonify(content), status.HTTP_200_OK


@iam_api.route('/iam/folder/', methods=['GET'])
def list_folders():
    content = state['iam']['folders']
    return jsonify(content), status.HTTP_200_OK


@iam_api.route('/iam/project/', methods=['GET'])
def list_projects():
    content = state['iam']['projects']
    return jsonify(content), status.HTTP_200_OK


@iam_api.route('/iam/identity/', methods=['GET'])
def list_identities():
    content = state['iam']['identities']
    return jsonify(content), status.HTTP_200_OK


@iam_api.route('/iam/role/', methods=['GET'])
def list_roles():
    content = state['iam']['roles']
    return jsonify(content), status.HTTP_200_OK


@iam_api.route('/iam/organization/<org>', methods=['POST'])
def create_organization(org):
    content = {}
    if org not in state['iam']['organizations'].keys():
        data = json.loads(request.data)
        state['iam']['organizations'][org] = data
        content = 'Organization: {0} created'.format(org)
        return content, status.HTTP_201_CREATED
    else:
        content = 'Organization: {0} already exists'.format(org)
        return content, status.HTTP_400_BAD_REQUEST


@iam_api.route('/iam/project/<project>', methods=['POST'])
def create_project(project):
    content = {}
    if project not in state['iam']['projects'].keys():
        data = json.loads(request.data)
        state['iam']['projects'][project] = data
        content = 'Project: {0} created'.format(project)
        return content, status.HTTP_201_CREATED
    else:
        content = 'Project: {0} already exists'.format(project)
        return content, status.HTTP_400_BAD_REQUEST


@iam_api.route('/iam/identity/<identity>', methods=['POST'])
def create_identity(identity):
    if identity not in state['iam']['identities'].keys():
        data = json.loads(request.data)
        state['iam']['identities'][identity] = data
        content = 'Identity: {0} created'.format(identity)
        return content, status.HTTP_201_CREATED
    else:
        content = 'Identity: {0} already exists'.format(identity)
        return content, status.HTTP_400_BAD_REQUEST


@iam_api.route('/iam/organization/<org>', methods=['DELETE'])
def remove_organization(org):
    if org in state['iam']['organizations'].keys():
        del state['iam']['organizations'][org]
        content = 'Organization: {0} deleted'.format(org)
        return content, status.HTTP_200_OK
    else:
        content = 'Organization: {0} does not exists'.format(org)
        return content, status.HTTP_400_BAD_REQUEST


@iam_api.route('/iam/folder/<folder>', methods=['DELETE'])
def remove_folder(folder):
    if folder in state['iam']['folders'].keys():
        del state['iam']['folders'][folder]
        content = 'Folder: {0} deleted'.format(folder)
        return content, status.HTTP_200_OK
    else:
        content = 'Folder: {0} does not exists'.format(folder)
        return content, status.HTTP_400_BAD_REQUEST


@iam_api.route('/iam/project/<project>', methods=['DELETE'])
def remove_project(project):
    if org in state['iam']['projects'].keys():
        del state['iam']['projects'][project]
        content = 'Project: {0} deleted'.format(project)
        return content, status.HTTP_200_OK
    else:
        content = 'Project: {0} does not exists'.format(project)
        return content, status.HTTP_400_BAD_REQUEST


@iam_api.route('/iam/role/<role>', methods=['DELETE'])
def remove_role(role):
    if org in state['iam']['roles'].keys():
        del state['iam']['roles'][role]
        content = 'Role: {0} deleted'.format(role)
        return content, status.HTTP_200_OK
    else:
        content = 'Role: {0} does not exists'.format(role)
        return content, status.HTTP_400_BAD_REQUEST
