import os
import yaml
import json
import queue
import shutil

from flask import jsonify
from flask import request
from flask import Blueprint

from os.path import expanduser

pubsub_api = Blueprint('pubsub_api', __name__)


"""Superchaind API: Pubsub."""
topics = {}
@pubsub_api.route('/pubsub/', methods=['GET'])
def pubsub_get():
    data = {
        'status': 'success',
        'topics': []
    }
    for topic in topics:
        data['topics'].append(topic)
    return jsonify(data)


@pubsub_api.route('/pubsub/<topic_name>', methods=['GET'])
def pubsub_read(topic_name):
    if topic_name in topics:
        topic = topics[topic_name]
        if not topic.empty():
            message = topic.get()
            data = {'status': 'success', 'messages': [message]}
        else:
            data = {'status': 'success', 'messages': []}
    else:
        data = {'status': 'error', 'message': 'Topic {0} does not exist'.format(topic_name)}
    return jsonify(data)


@pubsub_api.route('/pubsub/<topic_name>', methods=['POST'])
def pubsub_post(topic_name):
    data = json.loads(request.data)
    if data['OPERATION'] == 'CREATE_TOPIC':
        if not topic_name.isalnum():
            return jsonify({'status': 'error', 'message': 'Invalid topic name: {0}'.format(topic_name)})

        if topic_name in topics:
            return jsonify({'status': 'error', 'message': 'topic: {0} already exists'.format(topic_name)})

        topics[topic_name] = queue.Queue()
        data = {'status': 'success', 'message': 'Created topic {0}'.format(topic_name)}
        return jsonify(data)
    elif data['OPERATION'] == 'CREATE_SUBSCRIPTION':
        pass
    elif data['OPERATION'] == 'WRITE_MESSAGE':
        if topic_name in topics:
            topic = topics[topic_name]
            message = data['message']
            topic.put(message)
            data = {'status': 'success', 'message': 'Wrote message {0} to topic {1}'.format(message, topic_name)}
            return jsonify(data)
        else:
            data = {'status': 'error', 'message': 'Topic {0} does not exist'.format(topic_name)}
            return jsonify(data)
    else:
        data = {'status': 'error', 'message': 'Uknown operation'}
        return jsonify(data)


@pubsub_api.route('/pubsub/<topic_name>', methods=['DELETE'])
def pubsub_remove(topic_name):
    # sanitize path: no .. or /. It contains only chars and ints
    if not topic_name.isalnum():
        return jsonify({'status': 'error', 'message': 'Invalid topic name: {0}'.format(topic_name)})

    if topic_name not in topics:
        return jsonify({'status': 'error', 'message': 'Topic : {0} does not exist'.format(topic_name)})
    del topics[topic_name]
    return jsonify({'status': 'success', 'message': 'Removed topic: {0}'.format(topic_name)})
