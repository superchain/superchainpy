import yaml

from flask import jsonify
from flask import Blueprint

from os.path import expanduser

config_api = Blueprint('config_api', __name__)
config_file = expanduser("~/.superchain/config.yaml")


"""Superchaind API: Config."""
@config_api.route('/config', methods=['GET'])
def config_get():
    stream = open(config_file)
    config = yaml.load(stream)

    return jsonify(config)


@config_api.route('/config', methods=['POST'])
def config_post():
    data = json.loads(request.data)
    system, key = data['key'].split(':')
    value = data['value']

    stream = open(config_file)
    config = yaml.load(stream)

    config[system][key] = value

    stream = open(config_file, 'w')
    yaml.dump(config, stream)

    return jsonify(config)
