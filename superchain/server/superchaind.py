import os
import sys
import click
import dataset
import requests

from flask import Flask
from flask import request
from flask import jsonify

from superchain.server.server import initialize, Miner, Server


"""Console script for superchaind."""

@click.group()
def main(args=None): pass

@main.command()
def start():
    initialize()
    miner = Miner()
    miner.start()
    server = Server()
    server.start()

main.add_command(start)
