import time
import datetime
import threading


def create_genesis_block(data=None):
  return {
    'index': 0,
    'timestamp': datetime.datetime.now(),
    'data': data,
    'previous': '',
    'hash': 'HASH:{0}'.format(0)
  }

def Block(previous, data=None):
  return {
    'index': previous['index'] + 1,
    'timestamp': datetime.datetime.now(),
    'data': data,
    'previous': previous['hash'],
    'hash': 'HASH:{0}'.format(previous['index'] + 1)
  }

class Miner(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def stop(self):
        self._stop_event.set()

    def run(self):
        blockchain = []
        block = create_genesis_block()
        print(block)
        blockchain.append(block)
        while True:
            time.sleep(3)
            block = Block(blockchain[-1], 'new_transactions_...')
            print(block)
            blockchain.append(block)

if __name__ == '__main__':
    miner = Miner()
    miner.start()
