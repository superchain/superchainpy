import json

from flask import jsonify
from flask import request
from flask import Blueprint

from os.path import expanduser

wallet_api = Blueprint('wallet_api', __name__)


"""Superchaind API: Wallet."""
@wallet_api.route('/wallets/', methods=['GET'])
def wallet_get():
    return jsonify(state['wallets'])

@wallet_api.route('/wallets/', methods=['POST'])
def wallet_post():
    data = json.loads(request.data)
    # TODO: hardcoded response for now
    wallet = {
        'id': 'dasdasdasdasdsatd8692bg368',
        'owner': data['owner'],
        'addresses': ['address1', 'address2'],
        'OPCODE': 'CREATE'
    }
    memory['wallets'][wallet['id']] = wallet
    return jsonify(state['wallets'])

@wallet_api.route('/wallets/<wallet>', methods=['DELETE'])
def wallet_remove(wallet):
    return 'chain wallet remove {0}'.format(wallet)
