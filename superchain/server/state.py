"""Superchaind API: State."""

state = {
    'iam': {
        'identities': {},
        'organizations': {},
        'folders': {},
        'projects': {}
    }
}
