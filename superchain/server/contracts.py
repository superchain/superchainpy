import os
import yaml
import json
import shutil

from flask import jsonify
from flask import request
from flask import Blueprint

from os.path import expanduser

contracts_api = Blueprint('contracts_api', __name__)


"""Superchaind API: Contracts."""
@contracts_api.route('/contracts/', methods=['GET'])
def contracts_get():
    return "chain contracts list of contracts"

@contracts_api.route('/contracts/<contract>', methods=['POST'])
def contracts_post(contract):
    return 'chain contracts create {0}'.format(contract)

@contracts_api.route('/contracts/<contract>', methods=['DELETE'])
def contracts_remove(contract):
    return 'chain contracts remove {0}'.format(contract)
