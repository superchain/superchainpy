#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `superchain` package."""


import time
import json
import random
import string
import unittest
from click.testing import CliRunner

from superchain.cli import cli


class TestSuperchain(unittest.TestCase):
    """Tests for `Superchain` package."""

    def setUp(self):
        """Set up test fixtures, if any."""
        self.function = ''.join(random.choice(string.ascii_lowercase) for x in range(10))

    def test_cli_functions(self):
        runner = CliRunner()
        result = runner.invoke(cli.main, ['functions'])
        assert result.exit_code == 0
        result = runner.invoke(cli.main, ['functions', 'create', self.function, 'function.py'])
        assert result.exit_code == 0
        result = runner.invoke(cli.main, ['functions', 'ls'])
        assert result.exit_code == 0
        result = runner.invoke(cli.main, ['functions', 'rm', self.function])
        assert result.exit_code == 0

    def tearDown(self):
        """Tear down test fixtures, if any."""

if __name__ == '__main__':
    unittest.main()
