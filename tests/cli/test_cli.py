#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `superchain` package."""


import unittest
from click.testing import CliRunner

from superchain.cli import cli


class TestSuperchain(unittest.TestCase):
    """Tests for `Superchain` package."""

    def setUp(self):
        """Set up test fixtures, if any."""

    def test_cli(self):
        """Test the CLI."""
        runner = CliRunner()
        result = runner.invoke(cli.main)
        assert result.exit_code == 0
        help_result = runner.invoke(cli.main, ['--help'])
        assert help_result.exit_code == 0

    def tearDown(self):
        """Tear down test fixtures, if any."""

if __name__ == '__main__':
    unittest.main()
