#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `superchain` package."""


import time
import json
import random
import string
import unittest
from click.testing import CliRunner

from superchain.cli import cli


class TestSuperchain(unittest.TestCase):
    """Tests for `Superchain` package."""

    def setUp(self):
        """Set up test fixtures, if any."""
        self.name = ''.join(random.choice(string.ascii_lowercase) for x in range(10))

    def test_cli_identity(self):
        runner = CliRunner()
        result = runner.invoke(cli.main, ['identity'])
        assert result.exit_code == 0
        result = runner.invoke(cli.main, ['identity', 'ls', '--format', 'json'])
        import pdb; pdb.set_trace()
        assert result.exit_code == 0
        result = runner.invoke(cli.main, ['identity', 'create', self.name])
        assert result.exit_code == 0
        result = runner.invoke(cli.main, ['identity', 'rm', self.name])
        assert result.exit_code == 0

    def tearDown(self):
        """Tear down test fixtures, if any."""

if __name__ == '__main__':
    unittest.main()
