#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `superchain` package."""

import random
import string
import unittest
from click.testing import CliRunner

from superchain.cli import cli


class TestSuperchain(unittest.TestCase):
    """Tests for `Superchain` package."""

    def setUp(self):
        """Set up test fixtures, if any."""
        self.bucket = 'cs://' + ''.join(random.choice(string.ascii_lowercase) for x in range(10))

    def test_cli_storage(self):
        runner = CliRunner()
        result = runner.invoke(cli.main, ['storage'])
        assert result.exit_code == 0
        result = runner.invoke(cli.main, ['storage', 'create', self.bucket])
        assert result.exit_code == 0
        result = runner.invoke(cli.main, ['storage', 'ls'])
        assert result.exit_code == 0
        result = runner.invoke(cli.main, ['storage', 'rm', self.bucket])
        assert result.exit_code == 0

    def tearDown(self):
        """Tear down test fixtures, if any."""

if __name__ == '__main__':
    unittest.main()
