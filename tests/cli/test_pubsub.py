#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `superchain` package."""

import random
import string
import unittest
from click.testing import CliRunner

from superchain.cli import cli


class TestSuperchain(unittest.TestCase):
    """Tests for `Superchain` package."""

    def setUp(self):
        """Set up test fixtures, if any."""
        self.topic = ''.join(random.choice(string.ascii_lowercase) for x in range(10))

    def test_cli_pubsub(self):
        runner = CliRunner()
        result = runner.invoke(cli.main, ['pubsub'])
        assert result.exit_code == 0
        result = runner.invoke(cli.main, ['pubsub', 'create', self.topic])
        assert result.exit_code == 0
        result = runner.invoke(cli.main, ['pubsub', 'ls'])
        assert result.exit_code == 0
        result = runner.invoke(cli.main, ['pubsub', 'rm', self.topic])
        assert result.exit_code == 0

    def tearDown(self):
        """Tear down test fixtures, if any."""

if __name__ == '__main__':
    unittest.main()
