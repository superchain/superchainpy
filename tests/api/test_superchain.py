#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `superchain` package."""

import json
import unittest
import requests

from superchain.server import server

url = 'http://localhost:5000/{0}'

class TestSuperchain(unittest.TestCase):
    """Tests for `superchain` package."""

    def setUp(self):
        """Set up test fixtures, if any."""

    def test_superchain(self):
        """Test Superchain."""

        # test ping
        response = requests.get('http://localhost:5000/')
        assert response.json()['message'] == 'Hello, World!'

    def tearDown(self):
        """Tear down test fixtures, if any."""

if __name__ == '__main__':
    unittest.main()
