#!/usr/bin/env python

"""Tests for `pubsub` package."""

import time
import json
import random
import string
import unittest
import requests


url = 'http://localhost:5000/{0}'

class TestPubsub(unittest.TestCase):
    """Tests for `pubsub` package."""

    def setUp(self):
        """Set up test fixtures, if any."""
        self.topic = ''.join(random.choice(string.ascii_lowercase) for x in range(10))

    def test_pubsub(self):
        """Test Pubsub."""

        # create queue
        data = {
            'OPERATION': 'CREATE_TOPIC',
            'topic_name': self.topic,
        }
        response = requests.post(url.format('pubsub/{0}'.format(self.topic)), data=json.dumps(data))
        assert response.json() == {'message': 'Created topic {0}'.format(self.topic), 'status': 'success'}

        # list topics
        response = requests.get(url.format('pubsub/'))
        assert self.topic in response.json()['topics']

        # remove topic
        response = requests.delete(url.format('pubsub/{0}'.format(self.topic)))
        assert response.json() == {'message': 'Removed topic: {0}'.format(self.topic), 'status': 'success'}

    def tearDown(self):
        """Tear down test fixtures, if any."""

if __name__ == '__main__':
    unittest.main()
