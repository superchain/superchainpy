#!/usr/bin/env python

"""Tests for `storage` package."""

import time
import json
import random
import string
import unittest
import requests

from superchain.server import server


url = 'http://localhost:5000/{0}'

class TestStorage(unittest.TestCase):
    """Tests for `storage` package."""

    def setUp(self):
        """Set up test fixtures, if any."""
        self.bucket = 'cs://{0}'.format(''.join(random.choice(string.ascii_lowercase) for x in range(10)))

    def test_storage(self):
        """Test Storage."""

        # create buckets
        data = {
            'OPERATION': 'CREATE', 'path': self.bucket
        }
        response = requests.post(url.format('storage/'), data=json.dumps(data))
        assert response.json() == {'message': 'Created {0}'.format(self.bucket), 'status': 'success'}

        # list buckets
        params = {'path': 'cs://'}
        response = requests.get(url.format('storage/'), params=params)
        buckets = []
        for entry in response.json()['directory']:
            buckets.append(entry['path'])
        assert self.bucket in buckets

        # remove bucket
        data = {'path': self.bucket}
        response = requests.delete(url.format('storage/'), data=json.dumps(data))
        assert response.json() == {'message': 'Deleted {0}'.format(self.bucket), 'status': 'success'}

    def tearDown(self):
        """Tear down test fixtures, if any."""

if __name__ == '__main__':
    unittest.main()
