#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `superchain` package."""

import time
import json
import dill
import random
import string
import unittest
import requests

import function as f


url = 'http://localhost:5000/{0}'

class TestSuperchain(unittest.TestCase):
    """Tests for `superchain` package."""

    def setUp(self):
        """Set up test fixtures, if any."""

    def test_functions(self):
        """Test Superchain Functions."""
        self.function = ''.join(random.choice(string.ascii_lowercase) for x in range(10))

        # create function
        code = dill.dumps(f)
        data = {
            'OPERATION': 'CREATE_FUNCTION',
            'code': code
        }
        response = requests.post(url.format('functions/{0}'.format(self.function)), data=code)
        assert response.json() == {'message': 'Created function {0}'.format(self.function), 'status': 'success'}

        # list functions
        response = requests.get(url.format('functions/'))
        assert self.function in response.json()['functions']

        # invoke functions
        data = {
            'OPERATION': 'INVOKE_FUNCTION',
            'function': self.function,
            'x': 2,
            'y': 2
        }
        response = requests.post(url.format('functions/{0}'.format(self.function)), data=json.dumps(data)).json()
        assert response['status'] == 'success'
        assert response['result'] == 4

        # remove function
        response = requests.delete(url.format('functions/{0}'.format(self.function)))
        assert response.json() == {'message': 'Deleted function: {0}'.format(self.function), 'status': 'success'}

    def tearDown(self):
        """Tear down test fixtures, if any."""

if __name__ == '__main__':
    unittest.main()
