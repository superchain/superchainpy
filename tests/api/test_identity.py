#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `identity` package."""

import time
import json
import random
import string
import unittest
import requests


url = 'http://localhost:5000/{0}'

class TestIdentity(unittest.TestCase):
    """Tests for `identity` package."""

    def setUp(self):
        """Set up test fixtures, if any."""

    def test_identity(self):
        """Test Identity."""

        # test identity
        identity = ''.join(random.choice(string.ascii_lowercase) for x in range(10))
        response = requests.post(url.format('identity/{0}'.format(identity)))
        assert response.json() == {'identity': identity, 'status': 'success'}
        time.sleep(1)
        response = requests.get(url.format('identity'))
        assert response.json() == {'identities': {identity: {'OPCODE': 'CREATE'}}, 'status': 'success'}
        response = requests.delete(url.format('identity/{0}'.format(identity)))
        assert response.json() == {'identity': identity, 'status': 'success'}
        time.sleep(1)
        response = requests.get(url.format('identity'))
        assert identity not in response.json()['identities']

    def tearDown(self):
        """Tear down test fixtures, if any."""

if __name__ == '__main__':
    unittest.main()
