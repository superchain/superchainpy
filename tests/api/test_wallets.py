#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `wallet` package."""

import time
import json
import random
import string
import unittest
import requests


url = 'http://localhost:5000/{0}'

class TestWallet(unittest.TestCase):
    """Tests for `wallet` package."""

    def setUp(self):
        """Set up test fixtures, if any."""

    def test_wallets(self):
        """Test Wallets."""
        # test wallets
        response = requests.post(url.format('wallets/'), data=json.dumps({'owner':'alice'}))
        print(response)
        time.sleep(1)
        response = requests.get(url.format('wallets/')).json()
        assert response['dasdasdasdasdsatd8692bg368']['owner'] == 'alice'
        print(response)
        response = requests.delete(url.format('wallets/alice'))
        print(response)

    def tearDown(self):
        """Tear down test fixtures, if any."""

if __name__ == '__main__':
    unittest.main()
